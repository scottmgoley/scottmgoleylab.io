Tom Lee, Fundstrat

https://www.youtube.com/watch?v=GGberGnxiJk
Slides: https://www.slideshare.net/msuster/fundstrat-bitcoin-blockchain-presentation-for-upfront-summit


* digital trust
		- fiat requires government trust
		- correlation between lack of trust in government & crypto development / investment
		- 92% of millenials don't trust banks
![[Pasted image 20220202123519.png]]
![[Pasted image 20220202123545.png]]
		

* millenials drive change
	![[Pasted image 20220202123650.png]]

![[Pasted image 20220202123914.png]]

44% of RV buyers are millenials

![[Pasted image 20220202124347.png]]
![[Pasted image 20220202124432.png]]

housing appreciation
![[Pasted image 20220202124404.png]]

generational "store of value" selection
![[Pasted image 20220202124449.png]]

![[Pasted image 20220202125930.png]]

![[Pasted image 20220202131310.png]]

* crypto is an emergent asset class similar to emerging markets
![[Pasted image 20220202124214.png]]


* why wall street will care

![[Pasted image 20220202125529.png]]

![[Pasted image 20220202125603.png]]

![[Pasted image 20220202125632.png]]

![[Pasted image 20220202131018.png]]

![[Pasted image 20220202131033.png]]

