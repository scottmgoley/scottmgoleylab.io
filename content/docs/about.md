---
title: About Scott
subtitle: Or some lists of facts that are mildly relevent to what I've done so far.
comments: false
---


I'm Scott. I'm still building these pages so bear with me.


## Timeline
- 1990-1995: Born in Florida
- 1996-2000: Swimming, early homeschooling, and learning how to make the most of being the youngest.
- 2001-2004: Family purchased the iconic iMac in Bondi Blue.
- 2005: Watched brother build desktop pc. Aroused curiosity in linux, bios, and command line.
- 2008: Took first statistics course via dual-enrollment at local community college.
- 2011: Highschool graduation + AA degree. First year Junior at FSU pursuing Statistics.
- 2013: First exposure to cryptocurrency.
- 2014: First exposure to R and Python for analytics. Renewed interest in linux.
- 2015: Graduation from FSU. Move to Jacksonville. Took middle office equity trading job at Deutsche Bank (Capital Markets).
- 2017: Took a data & analytics position at Advanced Recovery Systems (Healthcare marketing).
- 2018: Took a fintech data & analytics position at Finexio (Financial Services Technology).
- 2021: Took a data & analytics position at Lumi. Layoff. Joined stealth startup in real estate PropTech.

## Education

 - AA Degree in Business from Pensacola State College
 - BS Degree in Psychology (Minor in Statistics) from Florida State University
 - Certificate in Data Analytics and SAS Programming from SAS foundation
 - Certificate in Salesforce Administration
 - Certificate in Heroku Architecture by Salesforce
 - Datacamp
    - Data Analyst with Python
    - Data Scientist with Python
    - Quantitative Analyst with R

## Favorite Quotes

 - "Of all sad words of tongue or pen, the saddest are these: It might have been." - John Greenleaf Whittier

 - "I am the captain of my fate, the master of my soul" - William Ernest Henley

 - "Do not go gentle into that good night" - Dylan Thomas

<!--- TODO: Add quotes --->
