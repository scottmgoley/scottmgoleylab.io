---
title: Now
subtitle: A Now Page
comments: false
---

### This is my "Now" Page

Here are a few things I'm focused on right now:

- I recently joined a stealth startup operating in as a data & analytics engineer. More details to come later!
- Active short-term to swing-term (not intraday) trading across crypto & us equities asset classes
- Listening to a few podcasts & reading a few books! My takeaway notes available from the side bar
- Answering questions related to data, dbt, and more on [StackOverflow](https://stackoverflow.com/users/3073340/sgoley)
- Reworking this site to be compatible and explorable via [obsidian](https://obsidian.md/), my latest obsession
- Banking my own entreprenurial ideas and growing some personal projects

### A Now Page?

This is a [/now page](https://nownownow.com/about) inspired by [Derek Sivers](https://sivers.org/).
