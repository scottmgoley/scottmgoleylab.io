---
title: Contact
subtitle: Some of the places around the internet you can reach me
comments: false
---


## Social Media

I don't really do much [social media](/posts/remember-when).

## Professional Profiles

<div class="github-card" data-github="sgoley" data-width="400" data-height="" data-theme="default"></div>
<script src="//cdn.jsdelivr.net/github-cards/latest/widget.js"></script>
</br>
{{< button href="https://github.com/sgoley" >}}GitHub{{< /button >}}
</br>
</br>
<a href="https://stackoverflow.com/users/3073340/sgoley"><img src="https://stackoverflow.com/users/flair/3073340.png?theme=dark" width="208" height="58" alt="profile for sgoley at Stack Overflow, Q&amp;A for professional and enthusiast programmers" title="profile for sgoley at Stack Overflow, Q&amp;A for professional and enthusiast programmers"></a>
</br>
{{< button href="https://stackoverflow.com/users/3073340/sgoley" >}}StackOverflow{{< /button >}}

## Business Inquiries

{{< button href="https://www.linkedin.com/in/sgoley/" >}}LinkedIn{{< /button >}}
