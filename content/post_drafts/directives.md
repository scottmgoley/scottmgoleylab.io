---
title: Directives
date: 2020-08-01
draft: true
tags: ["directives","learn"]
---

<!--- TODO: Finish --->

Heavily inspired by Sivers' iconic ["Just Tell me What to Do"](https://sive.rs/2do).

Directives are the *what* of life, while making the *why* available, but optional.

Great examples:

* [How to get rich](https://sive.rs/d1r)
* [How to like people](https://sive.rs/d1p)

My own:

* [How to eat](/posts/directives-food)
* [How to learn](/posts/directives-learn)
* [How to win](/posts/directives-win)
