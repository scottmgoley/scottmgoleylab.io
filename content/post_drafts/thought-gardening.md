---
title: Thought Gardening
date: 2020-09-29
draft: false
tags: ["psych", "growth"]
---

There is the idea that your mind should at all times remain focused and disciplined. That it should only allow certain thoughts in.
I can see where this idea comes from - it's useful to the industrial and educational paradigms that the US has been seeded with.
Personally, I've become a fan of what I call "thought gardening".

A simple version of the idea comes from Matthew 13:29-30:

> 29 ‘No,’ he said, ‘if you pull the weeds now, you might uproot the wheat with them. 30 Let both grow together until the harvest. At that time I will tell the harvesters: First collect the weeds and tie them in bundles to be burned; then gather the wheat into my barn.’

So to paraphrase that:

> If you immediately pull the bad thoughts out of your head, you will lose the goodness that can come from investigating them and knowing where they come from. The root then should be: investigate and know the sources of all your thoughts. Know if those thoughts have merit or are the idle chatter of your mind.

As mentioned in "Wherever You Go, There You Are"
