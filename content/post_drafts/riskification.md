---
title: Riskification
date: 2020-09-29
draft: false
tags: ["markets", "trading", "finance"]
---

Today in the gym an idea came to me as a seed and I've been thought-gardening it since:

> Everything is fundamentally a trade - but many things are effectively a compound trade.

For instance, if a trade is typically defined as entering a position by trading `Cash` for `Equity Shares in AAPL` or exiting `Corporate Notes` for `Cash` at current market value etc,
then what I'm interested in is all the additional layers of meaning that are built into that trade, the compound trades if you will.

So for instance, if I trade `X Cash` for `1 Equity Share in AAPL`, I am taking the following compound trade:

- Increasing my portfolio's long exposure vs AAPL returns
- Increasing my portfolio's risk exposure vs AAPL volatility, drawdown, etc.
- Increasing my portfolio's sector exposure vs Consumer Goods / Technology sector returns
- Increasing my portfolio's sector risk exposure vs the greater correlated Consumer Goods / Technology sector volatility, drawdown, etc.
- etc.

Now, I've long been familiar with the standard [risk-parity](https://en.wikipedia.org/wiki/Risk_parity) model for portfolio management. (I even interviewed with Bridgewater Associates)
What the main questions about that model are is:

- How are risks calculated?
- How are systemic risks calculated?

Because I imagine (and this is just a speculation since I've never worked inside such a fund) that risk was calculated from the history of the asset itself.
By implemention a quantitative risk model based on that asset or that asset's sector's performance etc.

However, I imagine that then applying and appropriate weighing the systemic effects of that asset's `compound trades` is where there was an entry point for bias from the developers.
Those biases are things that I've been questioning myself - things like:

- Do I have a personal bias towards `Cash`, for instance thinking it to be a place of financial security, and how does that bias become implemented in my trading?
- Does looking at the traditional perspective of "risk" history (volatility, drawdowns, etc.) of a particular asset actually give me that much information on future risk potential?
- Would a better perspective be to simply follow a model that focuses entirely on resource allocation accross compound risk components?

When starting to think along these lines, it occurs to me that perhaps the 5 greeks of simple American option contracts are in fact, not so complex after all.
