---
title: conformity
date: 2022-01-20
draft: true
tags: ["thinking"]
---

Natural selection operates at the fringe, eg. the breakthrough mutation start as a minority first.

As Naval rightly points out - group behavior is a consensus and has it's rewards of hierarchy ascendency, belonging, etc.

	
Social behavior is a market force with opposite sides being: 
1. The individual
2.  The group which offers a subsidy for conformity

Example are: 
* The bee & the hive
* The person to the society
* The country to Bretton Woods (subsidized world peace)


All groups operate on a consensus protocol. 
Group models include: 
- Centralized monopolies
	- Subject to gatekeeping
- Incentivize semi-centralized cooperatives
	- centralized for decision making, org efficiencies etc.
	- cartel formation
- Fully decentralized competitors
	- fragmented and inefficient
	- evolves to winner takes most
- fully decentralized cooperatives
	- assumption of specialized roles?
	- special interest capture?
	- staking incentives?
	- reputational history not enough because some rewards are worth total betrayal


Tradeoffs:

Total transparency = loss of anonymity
Total Opacity = loss of accountability
Best solution = multiple platforms with degree of anonymity / accountability