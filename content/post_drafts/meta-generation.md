---
title: The Meta Generation
date: 2020-06-03
draft: true
tags: ["demographics", "mygeneration", "games", "metagames"]
---

My generation grew up in a time of regime change - it's staple of our time.

Example: a popular online MOBA game, League of Legends has maintained around 100M monthly players on average since 2017.

A core feature of the sustained interest in the platform has been it powerful regime change dynamic: New characters, new items, new dynamics featured among it's ranked player base, new strategies communicated throughout by community prominent names across streaming channels and esports league.

This is the natural habitat of a generation of approximately 80M male, resource generating, low wealth, hungry, hackers who are currently existing to constantly break and re-break game dynamics in new and evolving ways.

And with every day that goes by, they bring more resource pressure to bear on the financials markets of the world.

------------

Let's investigate some of the "regimes" or "meta-games" of the US equities market in a semi-chronological order:

* The value meta game
* The aggregation meta game
* The privatized growth meta game
* The leverage game

------------

The value game:

A regime that was possible when the number of players within the financial markets was still small.
When the number of arbitragers was few to 0 such that slippage was generally not significant. When the availability of powerful tools for mass screening and analysis was low.

Then, if you did the work, you reaped the rewards that came with seeing value perform at the time. Even investing in pure beta (index investing) was more than enough to achieve +5-10% real returns after inflation during some periods. And because the nominals and multiples were all low, the returns could be significant without too much leverage.

Due to the technical barrier and lack of awareness, I would make the case that the amount of competition was relativativey low, causing opportunities to be plentiful enough that even some small losses were not unrecoverable.

Also - most things went public relatively quickly so new opportunity were frequently becoming available.

The value game was ended by the introduction of technology commoditizing the analysis of companies. Anything with strong cashflows and future prospects probably has strong support from capital markets.
As such, the opportunity is becoming more infrequently available (only during crashes, etc.)

------------

The aggregation game came from the need for the masses to generate returns without doing as much of the work. 
Some entrepreneurs (Vogel et al.) saw the opportunity to become 
