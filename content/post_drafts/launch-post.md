---
title: Launch post!
date: 2019-07-29
draft: false
---

In the interest of sharing some of the more eccentric cases of R, PowerBI, and many more of my frequently used platforms, I'd like to take an opportunity to share something of an official launch of my own personal content resource.

For the sake of convenience, you should be able to reach my articles, portfolio (in development), and more through a variety of links all pointing to the same place:

[justreadrtheinstructions.com](https://justreadrtheinstructions.com)

[scottgoley.com](https://scottgoley.com)

[scottmgoley.com](https://scottmgoley.com)

[sgoley.gitlab.io](https://sgoley.gitlab.io)

I've decided to call it "Just Readr the Instructions" combining two of my favorite universes - SpaceX and R's Tidyverse.

The idea, for which I am making this post as a call for public accountability, is to continuously broadcast my work in a reproducible way while also failing quite publicly into what I hope will become an eventual success much like the story of SpaceX.

As evidenced by the Google Analytics results image attached, there has already been a healthy interest in my only existing project currently documented on the site - my ODBC DirectQuery custom connector for PowerBI. I'm very much anticipating spending the time to expand this project, it's feature set, and it's documentation to fully meet the needs of the multiple users from around the world who have already contacted me in anticipation of a full release.

{{< figure src="/img/site/2019-07-27 17_26_05-Analytics.png" title="Google Analytics" >}}

I look forward to taking this starting point and disciplining myself to produce reliable and fundamental instructables for analytics and engineering problems that I may come across. To find new mentors and provide a similar role of mentorship to other data professionals a little earlier in their paths.

I can also say that even launching this has been quite the experience for me. Learning more about markdown and yaml than I ever expected, I've fully resolved all (more likely most) of my references, tags, missing images, and the various other placeholders I initially soft-launched this site with. If you are one of the reader's who dealt with these early issues - Thank you!

With many more resources on the way -

All the best,

Scott
