---
title: Heroku Certification Notes
date: 2019-03-26
draft: true
tags: ["heroku","bash","learn","hosting","salesforce","certification"]
---

<!--- TODO: Finish --->

# Table of Contents
1. [Term List](#term-list)
2. [Heroku Services](#heroku-services)
3. [FDW / Data Links from Shield Env](#shield)
4. [Heroku Connect](#connect)

To be completely honest, part of the reason I'm writing this doc is for my own study / self-reference as I prepare for Salesforces' Heroku Architecture Certification:
https://trailhead.salesforce.com/credentials/herokuarchitecturedesigner

Useful Quizlet resource:
https://quizlet.com/387365689/heroku-architecture-designer-flash-cards/


## Term List

https://devcenter.heroku.com/articles/glossary-of-heroku-terminology

Covers the majors like:
Add-on
App
Buildpack
Config var
Dyno

## Heroku Services

Deploy to Heroku

Pipelines

Heroku CI

Heroku Postgres
Heroku Redis
Apache Kafka on Heroku

Heroku Dashboard

Heroku Exec
`Heroku Exec is a feature for creating secure TCP and SSH tunnels into a dyno. It allows for SSH sessions, port forwarding, remote debugging, and inspection with popular Java diagnostic tools.`

Heroku Teams

Heroku SSL

Heroku Enterprise

Heroku Private Spaces

Shield Private Spaces

Heroku Connect

Salesforce Connect

Platform API

## FDW / Data Links from Shield Env



## Heroku Connect




Most likely the single most important section of the documentation will be this one:
https://devcenter.heroku.com/articles/writing-data-to-salesforce-with-heroku-connect#using-custom-triggers-with-heroku-connect

Which covers how to make custom triggers on the Heroku Connect Postgres side Schema, persistent and not recursive with the existing Connect action / update triggers.
