---
title: The Sivers Concept DB
draft: false
tags: ["organization", "database", "selfmanagement"]
---

https://youtu.be/JNkqvi29LBs?t=2636


The key here - give yourself an environment for reflecting on ideas later.
His version is to use a database of items, regularly loaded and tagged with keywords and categories.

By surrounding yourself with the *best* ideas on a particular topic, you are giving yourself the best creative scaffold for new thinking.

<script src="https://gist.github.com/sgoley/d7af07dce6534b22ce08a041e8287c20.js"></script>

Of course, I never just strictly imitated it but emulated it in my own way (Obsidian). More on that later!
