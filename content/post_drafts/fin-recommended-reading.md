---
title: Recommend Reading for new Investors
draft: false
tags: ["investing","tradingtools"]
---

* Why try to beat the markets?
		-  [In Investing, It’s When You Start And When You Finish](http://archive.nytimes.com/www.nytimes.com/interactive/2011/01/02/business/20110102-metrics-graphic.html?_r=2)
* Shouldn't I just buy and hold?
		- https://realinvestmentadvice.com/wait-for-the-fat-pitch-buy-and-hold-vs-active-management/
		- https://realinvestmentadvice.com/why-you-should-question-buy-and-hold-advice/
* What kind of environment are we operating in?
		- Macro (Demographic, Geopolitical) = [Zeihan Newsletter](https://us11.campaign-archive.com/home/?u=de2bc41f8324e6955ef65e0c9&id=5654564be1)
		- Financial (Eurodollar / post-Petrodollar) = [Snider Series](https://www.youtube.com/playlist?list=PL2jAZ0x9H0bR7K5mlncgcR1DujGsA0Ob8)
* What kind of market environment are we operating in?
		- Option driven public markets
			https://realinvestmentadvice.com/options-risk-in-the-stock-market/
			https://spotgamma.com/goldman-a-record-2-trillion-options-just-traded-what-it-means-for-fridays-massive-quad-witch-opex/
			Venture Capital is eating all most of the beta = [Research](https://cdn2.hubspot.net/hubfs/3925488/White%20Papers/Venture%20Returns%20Outperform%20Public%20Markets-AVG.pdf)
* What are the mechanics we need to be aware of to trade effectively?
		- [Short is Long](https://squeezemetrics.com/monitor/download/pdf/short_is_long.pdf)
		- [The Implied Order Book](https://squeezemetrics.com/download/The_Implied_Order_Book.pdf)
