---
title: Meta-Improvement
date: 2021-06-03
draft: false
tags: ["meta","learn","improvement","upskilling","powerleveling"]
---


Collection of resources about how to "improve" your self, your skill, or your process better.

* Almost all notable individuals in the modern society are the product of a team. Contributors who assist them in their styling, media, coaching, and more. You don't have to "make it" on your own. 
- Tim Ferriss advocates for recording yourself (video, audio, etc) if at all possible.
- Ray Dalio advocates for many specific things in ["Principles"](https://static.klipfolio.com/ebook/bridgewater-associates-ray-dalio-principles.pdf).
- Jordan Peterson discussing the idea of "Wishing upon a star" (https://www.youtube.com/watch?v=OIp_zeqWzmM) - set a direction, even a fuzzy direction that is subject to change.
* Use something for scheduling. Anything. I use [Clickup](https://clickup.com/) but use [whatever](https://apps.apple.com/us/app/habit-daily-tracker/id1445651730) [works](https://www.developgoodhabits.com/gamification-apps/) [best](https://pavlok.com/) for you. <sub>Not referral links, just examples.</sub>
