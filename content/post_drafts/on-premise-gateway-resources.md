---
title: On Premise Gateway Resource Links
date: 2019-07-29
draft: false
tags: ["gateway","onpremisegateway","powerbi"]
---

My submitted github issue for powershell command to set gateway anchor for cluster:

https://github.com/MicrosoftDocs/powerbi-docs/issues/1005

Powershell support for gateway clusters:

https://www.oreilly.com/library/view/mastering-microsoft-power/9781788297233/2a6f38cc-dfd9-4f77-95d4-d8c5ca84b7a1.xhtml

Radacad powerbi gateway:

https://radacad.com/the-power-bi-gateway-all-you-need-to-know

Definitive guide:

https://radacad.com/definitive-guide-to-power-bi-personal-gateway
