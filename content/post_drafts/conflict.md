---
title: The most underrated skill, conflict management
date: 2020-09-26
draft: true
tags: ["growth", "psych", "conflict"]
---

<!--- TODO: Finish --->

#### Intro

{{< details "Disclaimer" >}}
> This post is equal parts self exporation and discussion.

> I am not a therapist, psychological professional, or consoulor so please consult one as needed.

> Mental health, like your physical health, is not something that you should only wait and go to the emergency room for.

{{< /details >}}

On my journey towards emotional maturity, I've been forced to confront the reality that I'm enabling dysfuntional relationships or enabling poor team dynamics simply by avoiding necessary conflicts.
I've known for some time about my tendency to be conflict avoidant. It is one of the aspects of my personality that I've learned that I have to make adjustments around.
However, just making "adjustments" will eventually hold me back from future successes and needs to become a skill, rather than a deficiency.

I've always been a follower of mental models and frameworks. Emotional models that are very helpful include the emotional 'bid' put forward by Dr. John Gottman in "The Relationship Cure" or 'games' as put forward in Eric Berne's "Games People Play". I had no prior understanding of how to motivate myself to think differently in this situation but during a recent conversation about this dynamic, something connected. I'd like to make my own best attempt at putting forward one of my own:

#### "Conflict Debt"

Conflict debt is my idea that each unit of conflict accumulates over time (accrue in accounting terms) until they have been paid down.
For this mental model, I'm defining a single unit of conflict as one process of working through two or more incompatible realities or at least perceptions of reality.
I can have a conflict within myself (when choosing how to respond to a situation) or between myself and someone else (my perspective of what is acceptible vs theirs).
These conflicts can be handled through either heathly (empathy, active listening, compassion) or unhealthy (escalations, attacks, and triggers) mediums.
Now, I'm heavily influenced in this idea by the common concept in the technology domain of "technical debt" (as defined below):
{{< figure src="/img/random/what-is-technical-debt.png" title="Framework for Technical Debt" >}}

Leaning on the work of Dr. Gottman's insights about bids, we can build with this framework to put together a hypothesis of where relationships (personal or professional) fail.
Relationships first exist because they are of mutual benefit to both parties and have been so valued through a history of successful bids and bid results.
Then, to gain substance and "sticking power", they must not only be maintained through continued "bidding" but they must survive conflicts and no relationship is immune to all conflicts.
Conflict debt then is the outstanding conflict *liabilities* that exist which are not building relationship equity towards that history of successfully resolved conflicts.
Those resolved conflicts are very much an asset because it's a form of confidence to say "That was hard but we know we can get through it!"
The reality is that conflict management is usually untrained and that at best, we imitate the conflict management of our earliest and most significant influences - family, friends, abusers etc.

#### Paying Down Conflict Debt

While knowing that an amount of conflict debt exists or even doing an inventory of conflict debt is helpful - the reality is that debts not only accrue, they acrrue with interest.
The existence of conflict debt in a relationship causes a discount in any other investment into that relationship. As such, paying down debt is always necessary.
At best, a divorce or leaving a job can temporarily reset the balance, not unlike a financial reset from a bankruptcy declaration.
In reality, you are cheating yourself the opportunity to have long-standing deeply invested in relationships until such a time as the capacity is developed to preserve existing relationships by paying down the debt. Because I can only speak from my own experience, for me, re-opening accumulated conflict debt tends to happen all at once and leads to complex, disorganized, and extended conversations.
It is also why I have come to understand that paying down conflict debt is best done as frequently as possible in relationships.
I would assume that from personal experience, most can think of examples of this phenomenon: Ideally we'd like to bring up difficult things during good times, instead it seems easier to dredge up old greviences during the thick of an existing fight. Conflict debt will surface and resurface until it is finally and fully paid down for good both within the self and with any external parties.

{{< details "Resources" >}}

-  Eric Berne's "Games People Play"
-  Dr. John Gottman's "The Relationship Cure".

{{< /details >}}
