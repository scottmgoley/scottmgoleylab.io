---
title: Terms
date: 2020-08-01
draft: false
tags: ["terms", "definitions", "concepts"]
---

<!--- TODO: Finish --->

Important terms to something similar to Sivers' idea database:

-------------------------
`Russell Conjugation`
First encounter: https://www.edge.org/response-detail/27181
 > I am righteously indignant, you are annoyed, he is making a fuss over nothing.
-------------------------
`Kayfabe`
First encounter: https://www.edge.org/response-detail/11783
> In [professional wrestling](https://en.wikipedia.org/wiki/Professional_wrestling "Professional wrestling"), **kayfabe** [/ˈkeɪfeɪb/](https://en.wikipedia.org/wiki/Help:IPA/English "Help:IPA/English") (also called **work** or **worked**), as a noun, is the portrayal of staged events within the industry as "real" or "true", specifically the portrayal of competition, rivalries, and relationships between participants as being genuine and not staged.
-------------------------
`Overton window`
First encounter: https://waitbutwhy.com/2019/09/american-brain.html

> The **Overton window** is the range of policies politically acceptable to the mainstream population at a given time.[[1]](https://en.wikipedia.org/wiki/Overton_window#cite_note-1) It is also known as the **window of discourse**.
-------------------------
`Callback`
First encounter: 
> A **callback**, in terms of [comedy](https://en.wikipedia.org/wiki/Comedy "Comedy"), is a [joke](https://en.wikipedia.org/wiki/Joke "Joke") that refers to one previously told in the set.

-------------------------
`Banking`
First encounter: https://www.mixcloud.com/TheJoeRoganExperience/1256-david-lee-roth/ @ 

-------------------------
`Mimesis`
First encounter: https://www.youtube.com/watch?v=Ikk_ghhTauk

`Consilience`
First encounter: https://www.youtube.com/watch?v=NLyvKqT2U08
> In science and history, consilience is the principle that evidence from independent, unrelated sources can "converge" on strong conclusions.

`Embedded Growth Obligation`
First encounter:
Definition: https://theportal.wiki/wiki/Embedded_Growth_Obligations

`Load Bearing Fiction`
First encounter: 
> https://theportal.wiki/wiki/Load-Bearing_Fictions
>