---
title: On Landing a Rocket on an Autonomous Drone Ship
date: 2019-12-01
draft: false
tags: ["logo", "design", "meta"]
---

<!--- TODO: Finish --->

> "Do that which is meaningful over what is expedient."
>
> \- Jordan B. Peterson

When I originally set out to learn about data, statistics, and analytics, I did so because it basically ensured that I would have high quality employment for life.
If I had to dream of something of a stretch goal, I might have said that if I did incredibly well, I might land a position of exploring particularly interesting data in the financial sector for a startup or a hedge fund.

It has now been demonstrated to me that entrepreneurship can be far more than simply putting things together to make a dollar.
(See Chris Sacca's interview with Tim Ferriss and the dichotomy of East Coast vs West Coast investing)

I want to take the kinds of leaps that inspired SpaceX to conceptualize the idea of returning a rocket earth by means of a seagoing landing pad. And not just any landing pad, a fully autonomous drone ship called "Just Read the Instructions."

For that reason, I'm conceptualizing my personal brand as the crossover event of data + analytics meets everything that inspired the first landing which I distinctly remember seeing live:
https://www.youtube.com/watch?v=sYmQQn_ZSys

https://en.wikipedia.org/wiki/Falcon_9_first-stage_landing_tests

To speak to the data side as well, I'm a firm believer in the idea that data should be as accessible as possible. Thus discovering the "tidyverse" of Hadley Wickham was a key step in my own journey to analytical understanding. I attribute most of the mental models and frameworks that I currently use is built around tidy data and the formulations of data pipelines.

"Readr" in particular is a library to make ingesting into R as clear and direct a process as possible with a large selection of ingestable formats, datatypes, and more.

As a result: Just Readr the Instructions.

Legal notice: No copyright or trademark infringement intended, this will be used for representing this blog alone. No merchandise or additional usages will be conducted.
