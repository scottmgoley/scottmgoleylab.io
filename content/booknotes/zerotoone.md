https://fs.blog/peter-thiel-zero-to-one/

## 1. Each Moment Happens Once

Like Heraclitus, who said that you can only [step into the same river once](https://fs.blog/2013/10/heraclitus-fragments/), Thiel believes that **each moment in business happens only once.**

> The next Bill Gates will not build an operating system. The next Larry Page or Sergey Brin won’t make a search engine. And the next Mark Zuckerberg won’t create a social network. If you are copying these guys, you aren’t learning from them.
> 
> Of course, it’s easier to copy a model than to make something new. Doing what we already know how to do takes the world from 1 to n, adding more of something familiar. But every time we create something new, we go from 0 to 1. The act of creation is singular, as is the moment of creation, and the result is something fresh and strange.

## 2. There is no Formula

> The paradox of teaching entrepreneurship is that such a formula (for innovation) cannot exist; because every innovation is new and unique, no authority can prescribe in concrete terms how to be more innovative. Indeed, the single most powerful pattern I have noticed is that successful people find value in unexpected places, and they do this by thinking about business from first principles instead of formulas.

## 3. The Best Interview Question

> Whenever I interview someone for a job, I like to ask this question: “What important truth do very few people agree with you on?”
> 
> This is a question that sounds easy because it’s straightforward. Actually, it’s very hard to answer. It’s intellectually difficult because the knowledge that everyone is taught in school is by definition agreed upon. And it’s psychologically difficult because anyone trying to answer must say something she knows to be unpopular. Brilliant thinking is rare, but courage is in even shorter supply than genius.
> 
> Most commonly, I hear answers like the following:
> 
> “Our educational system is broken and urgently needs to be fixed.”
> 
> “America is exceptional.”
> 
> “There is no God.”
> 
> These are bad answers. The first and the second statements might be true, but many people already agree with them. The third statement simply takes one side in a familiar debate. A good answer takes the following form: “Most people believe in x, but the truth is the opposite of x.”

What does this have to do with the future?

> In the most minimal sense, the future is simply the set of all moments yet to come. But what makes the future distinctive and important isn’t that it hasn’t happened yet, but rather that it will be a time when the world looks different from today. … Most answers to the contrarian questions are different ways of seeing the present; good answers are as close as we can come to looking into the future.

## 4. A Company’s Most Important Strength

> Properly defined, a startup is the largest group of people you can convince of a plan to build a different future. A new company’s most important strength is new thinking: even more important than nimbleness, small size affords space to think.

> “Madness is rare in individuals—but in groups, parties, nations, and ages it is the rule.”
> 
> — Nietzche

## 5. The Contrarian Question

The question “What important truth do very few people agree with you on?” is hard to answer at first. It’s better to start with, “what does everybody agree on?”

> If you can identify a delusional popular belief, you can find what lies hidden behind it: the contrarian truth.
> 
> […]
> 
> Conventional beliefs only ever come to appear arbitrary and wrong in retrospect; whenever one collapses we call the old belief a bubble, but the distortions caused by bubbles don’t disappear when they pop. The internet bubble of the ‘90s was the biggest of the last two decades, and the lessons learned afterward define and distort almost all thinking about technology today. The first step to thinking clearly is to question what we think we know about the past.

Here is an example Thiel gives to help illuminate this idea.

> The entrepreneurs who stuck with Silicon Valley learned four big lessons from the dot-com crash that still guide business thinking today:
> 
> 1. **Make incremental advances** — “Grand visions inflated the bubble, so they should not be indulged. Anyone who claims to be able to do something great is suspect, and anyone who wants to change the world should be more humble. Small, incremental steps are the only safe path forward.”
> 
> 2. **Stay lean and flexible** — “All companies must be lean, which is code for unplanned. You should not know what your business will do; planning is arrogant and inflexible. Instead you should try things out, iterate, and treat entrepreneurship as agnostic experimentation.”
> 
> 3. **Improve on the competition** — “Don’t try to create a new market prematurely. The only way to know that you have a real business is to start with an already existing customer, so you should build your company by improving on recognizable products already offered by successful competitors.”
> 
> 4. **Focus on product, not sales** — “If your product requires advertising or salespeople to sell it, it’s not good enough: technology is primarily about product development, not distribution. Bubble-era advertising was obviously wasteful, so the only sustainable growth is viral growth.”
> 
> These lessons have become dogma in the startup world; those who would ignore them are presumed to invite the justified doom visited upon technology in the great crash of 2000. And yet the opposite principles are probably more correct.
> 
> 1. It is better to risk boldness than triviality.  
> 2. A bad plan is better than no plan.  
> 3. Competitive markets destroy profits.  
> 4. Sales matters just as much as product.”
> 
> To build the future we need to challenge the dogmas that shape our view of the past. That doesn’t mean the opposite of what is believed is necessarily true, it means that you need to rethink what is and is not true and determine how that shapes how we see the world today. As Thiel says, “The most contrarian thing of all is not to oppose the crowd but to think for yourself.

## 6. Progress Comes From Monopoly, not Competition

> The problem with a competitive business goes beyond lack of profits. Imagine you’re running one of those restaurants in Mountain View. You’re not that different from dozens of your competitors, so you’ve got to fight hard to survive. If you offer affordable food with low margins, you can probably pay employees only minimum wage. And you’ll need to squeeze out every efficiency: That is why small restaurants put Grandma to work at the register and make the kids wash dishes in the back.
> 
> A monopoly like Google is different. Since it doesn’t have to worry about competing with anyone, it has wider latitude to care about its workers, its products and its impact on the wider world. Google’s motto—”Don’t be evil”—is in part a branding ploy, but it is also characteristic of a kind of business that is successful enough to take ethics seriously without jeopardizing its own existence. In business, money is either an important thing or it is everything. Monopolists can afford to think about things other than making money; non-monopolists can’t. In perfect competition, a business is so focused on today’s margins that it can’t possibly plan for a long-term future. Only one thing can allow a business to transcend the daily brute struggle for survival: monopoly profits.
> 
> So a monopoly is good for everyone on the inside, but what about everyone on the outside? Do outsize profits come at the expense of the rest of society? Actually, yes: Profits come out of customers’ wallets, and monopolies deserve their bad reputation—but only in a world where nothing changes.
> 
> In a static world, a monopolist is just a rent collector. If you corner the market for something, you can jack up the price; others will have no choice but to buy from you. Think of the famous board game: Deeds are shuffled around from player to player, but the board never changes. There is no way to win by inventing a better kind of real-estate development. The relative values of the properties are fixed for all time, so all you can do is try to buy them up.
> 
> But the world we live in is dynamic: We can invent new and better things. Creative monopolists give customers more choices by adding entirely new categories of abundance to the world. Creative monopolies aren’t just good for the rest of society; they’re powerful engines for making it better.

## 7. Rivalry Causes us to Copy the Past

Marx and Shakespeare provide two models that we can use to understand almost every kind of conflict.

> According to Marx, people fight because they are different. The proletariat fights the bourgeoisie because they have completely different ideas and goals (generated, for Marx, by their very different material circumstances). The greater the difference, the greater the conflict.
> 
> To Shakespeare, by contrast, all combatants look more or less alike. It’s not at all clear why they should be fighting since they have nothing to fight about. Consider the opening to Romeo and Juliet: “Two households, both alike in dignity.” The two houses are alike, yet they hate each other. They grow even more similar as the feud escalates. Eventually, they lose sight of why they started fighting in the first place.”
> 
> In the world of business, at least, Shakespeare proves the superior guide. Inside a firm, people become obsessed with their competitors for career advancement. Then the firms themselves become obsessed with their competitors in the marketplace. Amid all the human drama, people lose sight of what matters and focus on their rivals instead.
> 
> […]
> 
> Rivalry causes us to overemphasize old opportunities and slavishly copy what has worked in the past.

## 8. Last can be First

> You’ve probably heard about “first mover advantage”: if you’re the first entrant into a market, you can capture significant market share while competitors scramble to get started. That can work, but moving first is a tactic, not a goal. What really matters is generating cash flows in the future, so being the first mover doesn’t do you any good if someone else comes along and unseats you. It’s much better to be the last mover – that is, to make the last great development in a specific market and enjoy years or even decades of monopoly profits.

Chess Grand-master José Raúl Capablanca put it well: to succeed, “you must study the endgame before everything else.”