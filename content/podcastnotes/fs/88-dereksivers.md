---
title: Derek Sivers on FS
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

<!--- TODO: Finish --->

## Synopsis:

> Musician, speaker, writer and entrepreneur, Derek Sivers chats about creating and running CD Baby, reading, mental models, living a meaningful life and that biggest mistake he’s ever made.

## Topics:



## Surprising Thoughts:

* How Derek reads books, highlight the surprising ideas

* Take on the responsibilities of courting attention. Maybe consider, stepping *into* the spotlight more instead of less.



## Embed:

<iframe width="560" height="315" src="https://www.youtube.com/embed/JNkqvi29LBs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
