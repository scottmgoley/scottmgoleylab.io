---
title: Lex Fridman on Huberman Lab
date: 2021-12-20
draft: false
tags: ["podcast","notes","intelligence","relationships"]
---


#### Summary

Dr. Lex Fridman PhD, is a scientist at MIT (Massachusetts Institute of Technology), working on robotics, artificial intelligence, autonomous vehicles and human-robot interactions. He is also the host of the Lex Fridman Podcast where he holds conversations with academics, entrepreneurs, athletes and creatives. Here we discuss humans, robots, and the capacity they hold for friendship and love. Dr. Fridman also shares with us his unique dream for a world where robots guide humans to be the best versions of themselves, and his efforts to make that dream a reality.


----------------


#### Timestamps:

-   00:00:00 Introduction: Lex Fridman
-   00:07:35 What is Artificial Intelligence?
-   00:26:46 Machine & Human Learning
-   00:32:21 Curiosity
-   00:36:55 Story Telling Robots
-   00:40:48 What Defines a Robot?
-   00:44:30 Magic & Surprise
-   00:47:37 How Robots Change Us
-   00:49:35 Relationships Defined
-   01:02:29 Lex’s Dream for Humanity
-   01:11:33 Improving Social Media
-   01:16:57 Challenges of Creativity
-   01:21:49 Suits & Dresses 
-   01:22:22 Loneliness
-   01:30:09 Empathy
-   01:35:12 Power Dynamics In Relationships
-   01:39:11 Robot Rights 
-   01:40:20 Dogs: Homer & Costello
-   01:52:41 Friendship
-   01:59:47 Russians & Suffering
-   02:05:38 Public vs. Private Life
-   02:14:04 How To Treat a Robot
-   02:17:12 The Value of Friendship
-   02:20:33 Martial Arts
-   02:31:34 Body-Mind Interactions
-   02:33:22 Romantic Love
-   02:42:51 The Lex Fridman Podcast
-   02:55:54 The Hedgehog
-   03:01:17 Concluding Statements

----------------

Surprising Ideas:

Variables of relationship between two individuals:

* Time
* Success / Failure
* Struggle
* Unstructured Time (waking up, doing day-to-day things)

The highlight on "Time" misses something - if you give a robot the concept of time, then how quickly could it understand its own capability for immortality and therefore adopt different goal weightings than our own because ours are inherently biased towards results within our own subjective perspective.