---
title: Dr. David Buss on Huberman Lab
date: 2021-12-30
draft: false
tags: ["podcast","notes","intelligence","relationships"]
---

#### Summary

In this episode, my guest is Dr. David Buss, Professor of Psychology at the University of Texas, Austin, and one of the founding members of the field of evolutionary psychology.

Dr. Buss describes his work on how people select mates for short and long-term relationships, the dynamics of human courtship, and mate value assessment — meaning how people measure up as potential partners. We also discuss the causes of infidelity and differences for infidelity in men and women. He explains how people evaluate and try to alter other people’s mate value as a means to secure and even poach mates. We discuss monogamous and non-monogamous relationships in humans. And we discuss what Dr. Buss calls “the dark triad”— features common in stalkers and narcissists that relate to sexual and psychological violence in relationships.

This episode is sure to be of interest to anyone single or in a relationship who seeks to know how people select mates and anyone who is interested in forming and maintaining healthy romantic partnerships.

------------------------

#### Link

<iframe width="560" height="315" src="https://www.youtube.com/embed/HXzTbCEqCJc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


----------------


#### Timestamps:

-   00:00:00 Introducing Dr. David Buss
-   00:04:10 Sponsors: ROKA, InsideTracker, Headspace
-   00:08:33 Choosing a Mate
-   00:13:40 Long Term Mates: Universal Desires
-   00:18:31 What Women & Men Seek in Long-Term Mates
-   00:25:10 Age Differences & Mating History 
-   00:32:20 Deception in Courtship
-   00:37:30 Emotional Stability
-   00:38:40 Lying About Long-Term Interest
-   00:41:56 Short-Term Mating Criteria, Sliding Standards & Context Effects
-   00:46:25 Sexual Infidelity: Variety Seeking & (Un)happiness & Mate Switching
-   00:54:25 Genetic Cuckolds, How Ovulation Impacts Mate Preference
-   00:57:00 Long-Term vs. Short-Term Cheating, Concealment
-   00:59:15 Emotional & Financial Infidelity
-   01:04:35 Contraception 
-   01:06:22 Status & Mating Success
-   01:10:10 Jealousy, Mate Value Discrepancies, Vigilance, Violence
-   01:24:13 Specificity of Intimate Partner Violence  
-   01:25:12 Mate Retention Tactics: Denigration, Guilt, Etc.
-   01:27:33 Narcissism, Machiavellianism, Psychopathy
-   01:33:25 Stalking
-   01:39:15 Influence of Children on Mate Value Assessments 
-   01:43:24 Attachment Styles, Mate Choice & Infidelity
-   01:46:40 Non-Monogamy, Unconventional Relationships  
-   01:54:00 Mate Value Self Evaluation, Anxiety About the Truth
-   02:02:12 Self Deception
-   02:05:35 The Future of Evolutionary Psychology & Neuroscience  
-   02:06:56 Books: When Men Behave Badly; The Evolution of Desire, Textbooks
-   02:10:42 Concluding Statements, Zero-Cost Support: Subscribe, Sponsors, Patreon, Thorne

------------------

Surprising Ideas:

