https://www.youtube.com/watch?v=nJkF2FdHAiM

flow of funds, z1

Stock buybacks can actually be going to employee share plans

S&P500 total shares outstanding decreased by 25 billion shares from 300B to 275B over 10 years which is a outstanding share reduction (due to stock buybacks) of approximately 1% per year.

Gross notional of stock buybacks is both C-Suite compensation & Employee Share purchase plans

Are stock buybacks the cause of income inequality?
or are S&P employees trying to "get in" on a raging bull market through their companies share purchase plans?

Ban on buybacks would result in: 
1. Companies ending employee share purchase plans
2. Having to do share / equity compensation in a more dilutive way

