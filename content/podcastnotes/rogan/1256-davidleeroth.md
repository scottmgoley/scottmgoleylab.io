---
title: David Lee Roth on The Joe Rogan Experience Ep1256
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
draft: false
---

#### How DLR writes a song

1:17:15
Link: https://podtail.com/en/podcast/the-joe-rogan-experience/-1256-david-lee-roth/
poem

1:19:00
Not "how do you know when you're done" , it's "How'd you get there?"

Running with the devil in 18 minutes

>If we're as good as we think we are, they'll come find us. 
>If we're not, they won't. 

1:26:13
* constantly bank your ideas 
	* A martial artist "banks" defensive moves or counter-attacks
	* Debate teams "bank" arguments prepared before they come to the debate

> Someone plays a song, and I can say "sounds like cowboy music"

1:35:00
> All the best pirates lauch, especially if its the last one
