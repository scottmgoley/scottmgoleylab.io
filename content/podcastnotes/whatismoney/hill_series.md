https://www.youtube.com/playlist?list=PL2jAZ0x9H0bQik_Rk4bOrL3QFl9rxmX7a

#### part 1

book recommendations: 
 - lila an inquiry into morals
 - human action
 - maps of meaning

Author, pirsig (zen and the art of motorcycle maintenance)
Blended philosophy of quality

Zen was an attempt to connect / broach the connection of Western Rationalism w/ Eastern Mysticism

Starting question, "What is Quality?"

Is it subjective that quality exists only in the eye of the beholder?
Is quality objective in that it exists within the object itself?

Metaphysics is the part of philosophy nature and structure of reality. 
Quality is closely related to the concept of value, values, and something have value.

Quality is the thing which inspires human action which action always communicates that an actor values performing the action more than not performing it.

The properties of a thing change with each fractal evolution of the thing itself. But generally retain alignment with the values expressed by previous evolutions and previous properties.
![[Pasted image 20220204151250.png]]

The layers of quality from low to high are: 

1. Inorganic
2. Biological
3. Social
4. Intellectual / Principal

![[Pasted image 20220204151313.png]]

Starting point for Pirsig was science and biochemistry. 

Scientific hypotheses was a "problem" due to its infinitely fractal nature that left science unable to meet it's goal of establishing "proven" knowledge.

* Background for Pirsig
* Enlightenment followed by "falling away" of meaning from reality ( interpreted as insanity)
* Subsequent electroconvulsive treatment

Revolution from Subject / Object metaphysics vs Qualitative metaphysics (MOQ)

Aristotilian Subject / Object duality forces a division to either: 
* objective (single interpretation)
* subjective (infinite interpretations)
![[Pasted image 20220204152815.png]]

Both sides remove agency and to a degree point towards nihilism.
Is there morality?

But in MOQ there is a way to synthesize both of these into tools for different elements of a fractal analysis of the whole.

![[Pasted image 20220204153232.png]]

Probabalistic universe. 
Stacked "preference" of such highly probabilistic outcomes that they appear to be discrete, consequential outcomes.

![[Pasted image 20220204153730.png]]

Humans are capable of working with both because of the right, left brain divide:
![[Pasted image 20220204153743.png]]

Voting patterns as an example of discrete outcomes made up of trends in continuous participants (voters). 


Substance & Causation are the natural ends of Aristotilian Metaphysics which its own methods arive at with no reconciliation for.

Substance is a useful "load-bearing fiction"

`A Causes B`
vs
`B values pre-condition A`

 patterns of value all the way down through the layers of analysis.

_____________________________________
#### part 2

Quality doesn't have to be defined. 
You understand it without definition.

> Quality is a direct experience independent of and prior to intellectual abstractions

The propensity of individuals to polarize towards either the left or the right hemisphere of the brain

![[Pasted image 20220204155919.png]]

Left is more static (division), Right is trying to achieve unity ()



> to tear down a factory or to revolt against a government or to avoid repair of a motorcycle because it is a system is to attack effects rather than causes; and as long as the attachk is upon effects only, no change is possible.

> the true system, the real system, 

value is primary

two categories: 
1. dynamic quality (heaven)
2. static quality (earth)

Humans are the mediators between dynamic and static quality 

For every subjective and objective pairing, their is also a transjective. 

![[Pasted image 20220204161342.png]]

The person who dances is not a dancer until they start doing the action of dancing. 
The floor on which is danced does not become a dancefloor until the person casts that as the play to dance.

Ghut / God root word is the return / exchange. 



Pain drives the reconfiguration of life and patterns of organization

---------------------------

Appearance of random, actuality is semi-predictable patterns

Music = harmony with the patterns

Marriage of "value of the individual" from european / christian heritage together with the great plains & freedom mentality of the native americans.

