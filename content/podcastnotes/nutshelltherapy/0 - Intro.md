---
title: Therapy in a Nutshell - Intro
date: 2021-06-10
draft: false
tags: ["therapy", "Explore"]
---

### Summary

Therapy is a professional emotional processing guide helping you implement common strategies and see common gotcha's in emotional processing.

Think of your therapist like a coach, you can listen to them or not but they are just trying to give you the best input they can.

------------------

### Takeaways

* Emotional Processing is like operating a fish processing factory.
* New fish is always coming in. You have to go through all the steps of processing it as it comes it or it will sit and get old & stinky.
* Identify -> Pause -> Explore -> Choose
* Emotional processing is a skill that can be learned
* Emotions are deeper than thoughts or behaviors - you can't just "think" your way out of emotions. Maybe temporarily but not lasting.

------------------

### Exercises

N/A
