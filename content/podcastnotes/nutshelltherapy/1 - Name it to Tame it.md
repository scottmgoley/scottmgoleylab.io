---
title: Therapy in a Nutshell - Name it to Tame it
date: 2021-06-10
draft: false
tags: ["therapy", "Explore"]
---

Naming your emotions.

When you aren't able to name your emotions, you can become reactive and impulsive since you don't have a target or direction to resolve those feelings.

This can cause a chain-reaction of additional negative emotion.

Ask: What am I feeling?
Is it anger, is it depression, is it sadness?
Use an emotion chart to precisely select for specific feelings. Name them.

------------------

Takeaways:

First step to processing emotions:
Notice, name and express them in words.

When you don't know what you're feeling it makes you helpless to act on them.

------------------

Exercises:

* Mood Tracking / journaling. Write down all the emotions you are currently feeling each time the alarm goes off.
* Set 3 goals for this course
