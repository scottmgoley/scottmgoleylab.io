---
title: Therapy in a Nutshell - Primary & Secondary Emotions
date: 2021-08-29
draft: false
tags: ["therapy", "Explore"]
---

What are secondary emotions - an emotion about an emotion rather than an emotion about an event.

Ex.
Bad grade == primary emotion
Shame & Anger == secondary emotion about reaction to bad grade because I knew I could have done better.

Saying things out loud can help to "express" the emotion but don't address underlying reasons.

Ex.
"I hate my boss" - Just a stress response
"Am I not good enough?" -
"Do I need to set better boundaries?" -


------------------

Takeaways:


| Primary emotions | Secondary emotions    |
|----------------- | --------------------- |
|  Instinctive     | Learned               |
|  Feel            | Control / Numb        |
|  Deeper          | Pushing (suppressive) |

* Secondary emotions give the illusion of control without understanding the actual underlying emotion and its true cause
* Secondary emotions lack intentionality

Examples of Primary & Secondary pairings:

Jealousy blames others for doing well, but protecting you from responsibility of trying hard but leaves you bitter.
Resentment protects you from connection and the risk of being hurt but leaves you lonely.
Blame protects you from responsibility & guilt but leaves you feeling helpless.
Hopelessness protects you from having to keep trying, but leaves you depressed and unaccomplished.

------------------

Exercises:

*
*
