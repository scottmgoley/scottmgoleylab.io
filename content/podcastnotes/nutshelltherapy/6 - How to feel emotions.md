---
title: Therapy in a Nutshel - How to feel emotions
date: 2021-08-29
draft: false
tags: ["therapy", "Explore"]
---

### The emotional brick

Holding emotions at a distance is straining much like holding a brick away from your body.
The longer you hold it away from your body (to prevent yourself from being close to it), the more tired you will be


Sometimes bringing your emotions in close, allowing yourself to recognize them and feel them, is necessary to releasing the weight of them.


---------------------------------
Emotions start with willingness to feel the emotions.

1. Describe, don't judge
	All core emotions serve a purpose
	This is what I'm experiencing right now, so let me be present to it.
2. Be curious
	Curiosity is an antidote to worry
	"Where am I feeling this?" "Does it *X*?"
	Stay in your body, don't get lost in your head or the abstract.
3. Lean in, leave the struggle for other things
4. Be present
	Don't pay attention to time, try to accept the moment.
5. Get back in your body.
	Breathe.
