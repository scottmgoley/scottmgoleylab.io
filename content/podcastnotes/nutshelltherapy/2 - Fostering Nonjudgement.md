---
title: Therapy in a Nutshell - Fostering Nonjudgemental Attitude
date: 2021-06-23
draft: false
tags: ["therapy", "Explore"]
---

### Taoist Farmer Story

> There was once a farmer in ancient China who owned a horse. “You are so lucky!” his neighbours told him, “to have a horse to pull the cart for you.” “Maybe,” the farmer replied.
> One day he didn’t latch the gate properly and the horse ran away. “Oh no! That is terrible news!” his neighbours cried. “Such bad luck!” “Maybe,” the farmer replied.
> A few days later the horse returned, bringing with it six wild horses. “How fantastic! You are so lucky,” his neighbours told him. “Maybe,” the farmer replied.
> The following week the farmer’s son was breaking-in one of the wild horses when it threw him to the ground, breaking his leg. “Oh no!” the neighbours cried. “Such bad luck, all over again!” “Maybe,” the farmer replied.
> The next day soldiers came and took away all the young men to fight in the army. The farmer’s son was left behind. “You are so lucky!” his neighbours cried. “Maybe,” the farmer replied.

------------------

Takeaways:

* Casting emotions positively or negatively skews our ability to process them.
* Teeter totter of suppress / react negative emotions through a cycle of repression and explosion

------------------

Exercises:

* Which emotions were you taught to believe are good / allowable
* Which emotions were you taught to believe are bad / un-allowable
