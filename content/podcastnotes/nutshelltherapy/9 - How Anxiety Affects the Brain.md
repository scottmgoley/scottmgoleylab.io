---
title: Therapy in a Nutshell - How Anxiety Affects the Brain
date: 2021-06-01
draft: true
tags: ["therapy", "Explore"]
---

9/30 How to Recognize and Turn off the Fight/Flight/Freeze Response

------------------
Takeaways:

Limbic system is basis of relationships in mammals
Anger, fear, jealousy
All these emotions are purposeful but if not controlled can overpower our desired behavior.

Fainting == regression to the brain stem level of the brain
FIght / Flight / Freeze == regression to the limbic level of the brain

Anxiety shows up in the body

Fight = Anger, blame, defensiveness, critical.
Flight = Avoiding, evasive.
Freeze = Detached, heavy, frozen, numb.
	Fawn = Submissive, self-blame,

Short bursts
Stress response is safe in the short term but can get trapped if we don't resolve the underlying perceived threat.

Typically now that these are more complex situations (social or relational) which we have a negative cascade effect as we become more effected by the stressor and therefore respond worse.

I.E. going on tilt.


------------------
Exercises:

* Become more aware of your individual triggers
* Notice your particular body signs
* Notice that you are "Flooding"
* Pause - take an observer position
* Try to calm down
* Ask for a break
* Commit to coming back
* Self Soothe (grounding exercise, walk, etc.)
* Come back with a calmer perspective
