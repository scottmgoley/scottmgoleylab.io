---
title: Slipping the DISC on The Portal w/ Eric Weinstein
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

## Synopsis:

> This "housekeeping" (cough cough) episode of The Portal is only for the hard core listeners who launched this experiment with us. This year we begin to take on the idea of the Distributed Idea Suppression Complex or "DISC".


## Topics:


## Surprising Thoughts:



## Embed:

<iframe src="https://www.art19.com/shows/the-portal/episodes/89a8a66b-c3f4-4401-9fbb-0a3051fdbb44/embed" style="width: 100%; height: 200px; border: 0 none;" scrolling="no”></iframe>
