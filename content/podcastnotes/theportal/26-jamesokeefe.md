---
title: James O'Keefe on The Portal w/ Eric Weinstein
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

## Synopsis:

> James O'Keefe is a dangerous man. He records people without their knowledge and publishes the results using the full power of our technological toolkit. He is well versed in the details of the law as to what can and cannot be legally recorded and/or published without the consent or even awareness of his targets. He is willing to risk prison to capture his stories and he has developed a policy of not settling out of court, even when it would be financially advantageous to do so. Clearly, he is willing to risk ruin and hatred for what he is doing, and is therefore not a man to be lightly trifled with.

## Topics:


## Surprising Thoughts:



## Embed:

<iframe src="https://www.art19.com/shows/the-portal/episodes/e74abda9-7570-4298-a661-1a81d7941c58/embed" style="width: 100%; height: 200px; border: 0 none;" scrolling="no”></iframe>
