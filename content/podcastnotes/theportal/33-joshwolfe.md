---
title: Josh Wolfe on The Portal w/ Eric Weinstein
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

## Synopsis:

>  Josh Wolfe of Lux Capital is a leading mind in the current wave of venture capitalists making waves. While aware of each other for some time, in this episode Josh sits down with Eric for their first meeting. Together the two explore various topics from comparing notes on their encounters with Jim Watson of DNA fame, to talking about what society gets wrong and how to see that as a source of opportunity.

## Topics:


## Surprising Thoughts:



## Embed:

<iframe src="https://www.art19.com/shows/the-portal/episodes/7d086129-e19d-4e17-8a6b-2dcd741c5f50/embed" style="width: 100%; height: 200px; border: 0 none;" scrolling="no”></iframe>
