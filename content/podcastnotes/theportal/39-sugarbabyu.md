---
title: Sugar Baby University on The Portal w/ Eric Weinstein
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

## Synopsis:

> One of the pillars of the American Dream has been that of seeing your children go to college. And, for the many families that can't afford the cost of soaring university tuitions, a new controversial institution has arisen to address the problem. That institution is Sugar Baby University, a tuition assistance campaign that attempts to allow attractive young women, and a smaller number of handsome young men, find generous older men to date in the quest to complete a new version of the American Dream by graduating debt free in an era which has made it all but impossible to discharge student debt even in personal bankruptcy since 2005.

## Topics:


## Surprising Thoughts:



## Embed:

<iframe src="https://www.art19.com/shows/the-portal/episodes/44792a9e-54d8-4100-b29d-efc84c3ed7d3/embed" style="width: 100%; height: 200px; border: 0 none;" scrolling="no”></iframe>
