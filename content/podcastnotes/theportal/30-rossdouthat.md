---
title: Ross Douthat on The Portal w/ Eric Weinstein
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

## Synopsis:

> Just before the great indooring due to the Pandemic of 2020, Eric sat down with conservative New York Times columnist Ross Douthat to discuss his book "The Decadent Society: How We Became the Victims of Our Own Success." Over champagne flutes filled with bubbly, the two discussed the various ways that the success and excesses of American Capitalism were now distorting the American Dream into a dystopian fever vision, making it far harder to wake up from this stasis in time to avoid the previous fates of fallen empires.

## Topics:


## Surprising Thoughts:



## Embed:

<iframe src="https://www.art19.com/shows/the-portal/episodes/82501abd-e52a-429f-9b7e-40889742f0a0/embed" style="width: 100%; height: 200px; border: 0 none;" scrolling="no”></iframe>
