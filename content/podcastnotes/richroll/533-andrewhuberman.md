---
title: Dr. Andrew Huberman on Rich Roll
date: 2020-07-05
tags: ["podcasts", "podcastnotes"]
---

<!--- TODO: Finish --->

## Synopsis:

> A neuroscientist and tenured professor in the Department of Neurobiology at Stanford University School of Medicine, Andrew specializes in neuroplasticity--the brain's ability to reorganize and repair itself by forming new neural connections throughout life.

## Topics:

- 1:26 - Andrew’s Background
- 15:29 - How Andrew Turned His Life Around
- 23:47 - How Andrew Combines Neuroscience with His Past
- 26:43 - The Human Brain: Internal State & External State
- 35:06 - Neural Plasticity - Steering the Nervous System
- 42:25 - State of Flow - The Dopamine System
- 59:07 - Process of Internal Rewards
- 1:05:55 - Studying Fear, Courage, and Resilience
- 1:16:03 - How to Deal with Problems of Motivation and Focus
- 1:24:41 - Panoramic Vision vs Focal Vision
- 1:30:45 - Methods of Focus and Deep Rest
- 1:34:50 - Optic Flow and EMDR
- 1:38:50 - Andrew’s Work in Addiction
- 1:47:06 - A Bio-Marker for Addicts to Avoid Relapse
- 1:49:36 - Neuroscience Perspective on Political Polarization
- 1:59:15 - The Importance of Internal Control
- 2:04:57 - Is There Hope for Us?

## Surprising Thoughts:

- Internal metronome raises our stress level when internal and external stimulus is mismatched.

## Embed:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SwQhKFMxmDY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
